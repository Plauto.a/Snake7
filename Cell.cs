﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace snake7
{
    public class Cell
    {
        public int Id { get; private set; }
        public int SnakeSize { get; private set; }
        private int[] values;
        private int gridSide;
        private int curDept;

        private int curSum;
        private int[] ids;
        private Dictionary<int, List<Snake>> SnakePit;

        public Cell(int idIn, int[] valuesIn)
        {
            Id = idIn;
            values = valuesIn;
            gridSide = Convert.ToInt32(Math.Sqrt(values.Length));

            SnakePit = new Dictionary<int, List<Snake>>();
        }

        public Dictionary<int, List<Snake>> Compute(int snakeSize)
        {
            SnakeSize = snakeSize;

            ids = new int[snakeSize];
            curDept = 0;
            ids[0] = Id;
            ComputeRecursive(Id, SnakeSize - 1);
            return SnakePit;
        }

        private void ComputeRecursive(int curId, int maxDept)
        {
            curSum += values[curId];
            curDept++;
            if (curDept <= maxDept)
            {
                if ((curId + 1) % gridSide != 0 && (curId + 1) < values.Length)
                {
                    int rId = curId + 1;
                    if (Max2Adjacents(rId, curDept))
                    {
                        ids[curDept] = rId;
                        ComputeRecursive(rId, maxDept);
                    }
                }

                if ((curId - gridSide) >= 0)
                {
                    int uId = curId - gridSide;
                    if (Max2Adjacents(uId, curDept))
                    {
                        ids[curDept] = uId;
                        ComputeRecursive(uId, maxDept);
                    }
                }

                if ((curId - 1) % gridSide != gridSide - 1 && (curId - 1) >= 0)
                {
                    int lId = curId - 1;
                    if (Max2Adjacents(lId, curDept))
                    {
                        ids[curDept] = lId;
                        ComputeRecursive(lId, maxDept);
                    }
                }

                if ((curId + gridSide) < values.Length)
                {
                    int dId = curId + gridSide;
                    if (Max2Adjacents(dId, curDept))
                    {
                        ids[curDept] = dId;
                        ComputeRecursive(dId, maxDept);
                    }
                }
            }
            else
            {
                Snake aSnake = new Snake(ids, curSum);
                List<Snake> snakesGroup;
                if (SnakePit.TryGetValue(curSum, out snakesGroup))
                {
                    snakesGroup.Add(aSnake);
                }
                else
                {
                    snakesGroup = new List<Snake>();
                    snakesGroup.Add(aSnake);
                    SnakePit.Add(curSum, snakesGroup);
                }
            }
            curSum -= values[curId];
            curDept--;
        }
        private bool Max2Adjacents(int newId, int dept)
        {
            int numAdj = 0;
            for (int i = 0; i < dept; i++)
            {
                if (newId == ids[i])
                {
                    return false;
                }
                if (newId == ids[i] + 1)
                {
                    numAdj++;
                }
                if (newId == ids[i] - 1)
                {
                    numAdj++;
                }
                if (newId == ids[i] + gridSide)
                {
                    numAdj++;
                }
                if (newId == ids[i] - gridSide)
                {
                    numAdj++;
                }
            }
            if (numAdj <= 1)
            {
                return true;
            }
            return false;
        }
    }
}
