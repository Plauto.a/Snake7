﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace snake7
{
    class Program
    {
        static int[] values;

        static void Main(string[] args)
        {
            if (!ReadInputFilePath())
            {
                return;
            }

            SnakeChallenge theChallenge = new SnakeChallenge(values, 7);
            SnakeCouple theAnswer = theChallenge.Compute();

            if (theAnswer != null)
            {
                string output = theAnswer.ToString() + Environment.NewLine;
                output += "Cells Visited: " + theChallenge.CellsVisited;
                FileUtils.SaveOutputFile(output);
                Console.WriteLine(output);
            }
            else
            {
                string output = "FAIL" + Environment.NewLine;
                FileUtils.SaveOutputFile(output);
                Console.WriteLine(output);
            }
            Console.ReadKey();
        }

        private static bool ReadInputFilePath()
        {
            Console.WriteLine("Input file path:");
            string filePath = Console.ReadLine();
            try
            {
                FileUtils.TryReadCSVFile(filePath, out values);
            }
            catch (Exception ex)
            {
                Console.WriteLine("You did not input a valid csv file path: " + Environment.NewLine + ex.Message);
                Console.ReadKey();
                return false;
            }
            return true;
        }
    }
}
