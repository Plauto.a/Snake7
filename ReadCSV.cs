﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace snake7
{
    public static class FileUtils
    {
        public static bool TryReadCSVFile(string filePath, out int[] values)
        {
            var lines = File.ReadLines(filePath);
            int linesNumber = lines.Count<string>();

            values = new int[linesNumber * linesNumber];

            int lineCount = 0;
            foreach (var line in lines)
            {
                string[] splitInts = line.Split(',');
                for (int i = 0; i < splitInts.Length; i++)
                {
                    values[(lineCount * linesNumber) + i] = Convert.ToInt32(splitInts[i]);
                }
                lineCount++;
            }
            return true;
        }

        public static void SaveOutputFile(string outputMessage)
        {
            using (StreamWriter writetext = new StreamWriter("output.txt"))
            {
                writetext.WriteLine(outputMessage);
            }
        }

    }
}
