﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace snake7
{
    public class Snake
    {
        private int sum;
        public int[] cellsIds;

        public Snake(int[] ids, int sumIn)
        {
            sum = sumIn;
            cellsIds = new int[ids.Length];
            Buffer.BlockCopy(ids, 0, cellsIds, 0, ids.Length * 4);
        }

        public override string ToString()
        {
            string output = "[" + cellsIds[0].ToString() + "][";
            for (int i = 1; i < cellsIds.Length - 1; i++)
            {
                output += cellsIds[i].ToString() + "][";
            }
            output += cellsIds[cellsIds.Length - 1].ToString() + "] = " + sum;

            return output;
        }

        public static bool Intersect(Snake snk1, Snake snk2)
        {
            bool intersect = false;
            for (int i = 0; i < snk1.cellsIds.Length; i++)
            {
                for (int j = 0; j < snk2.cellsIds.Length; j++)
                {
                    if (snk1.cellsIds[i] == snk2.cellsIds[j])
                    {
                        intersect = true;
                    }
                }
            }
            return intersect;
        }
    }

    public class SnakeCouple
    {
        public Snake SnakeA { get; set; }
        public Snake SnakeB { get; set; }

        public SnakeCouple(Snake snkA, Snake snkB)
        {
            SnakeA = snkA;
            SnakeB = snkB;
        }

        public override string ToString()
        {
            string output = string.Empty;
            if (SnakeA != null && SnakeB != null)
            {
                output = "Couple Found: " + Environment.NewLine;
                output += SnakeA.ToString() + Environment.NewLine;
                output += SnakeB.ToString() + Environment.NewLine;
            }
            
            return output;
        }   
    }
}
