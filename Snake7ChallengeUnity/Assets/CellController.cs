﻿using UnityEngine;

public class CellController : MonoBehaviour 
{
    public int status = 0;

    public int id;
    public int value;

    private SpriteRenderer cellRenderer;
    void Awake()
    {
        cellRenderer = GetComponent<SpriteRenderer>();
    }

    public void SetColor(Color color)
    {
        cellRenderer.color = color;
    }

}
