﻿using UnityEngine;
using System.Collections.Generic;

public class GridController : MonoBehaviour 
{
    public List<CellController> cells;

    public GameObject cellPrefab;
    public GameObject cellsCenter;

    public void SetCellValues(int[] valuesIn)
    {
        cells = new List<CellController>();
        int side = (int)Mathf.Sqrt(valuesIn.Length);
        for (int i = 0; i < side; i++)
        {
            for (int j = 0; j < side; j++)
            {
                GameObject aCell = GameObject.Instantiate(cellPrefab, new Vector3(j * .14f, i * -.14f, 0), Quaternion.identity, cellsCenter.transform);
                CellController aCellCtrl = aCell.GetComponent<CellController>();
                aCellCtrl.id = (i * side) + j;
                aCellCtrl.value = valuesIn[(i * side) + j];
                if (aCellCtrl != null)
                {
                    cells.Add(aCellCtrl);
                }
            }
        }

        float midPoint = (side / 2) * .14f;
        cellsCenter.transform.position = new Vector3(-midPoint, midPoint, 0);
    }

    public void SetSnake(int[] snake, Color color)
    {
        for (int i = 0; i < snake.Length; i++)
        {
            cells[snake[i]].SetColor(color);
        }
    }
}
