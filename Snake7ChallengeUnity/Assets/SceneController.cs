﻿using UnityEditor;
using UnityEngine;
using snake7;
using System.IO;
using System.Linq;
using System;
using System.Collections;

public class SceneController : MonoBehaviour 
{
    GridController gridCtrl;
    int[] valTest;
    void Awake()
    {
        int side = 100;
        //Random rnd = new UnityEngine.Random();
        gridCtrl = GetComponentInChildren<GridController>();
        valTest = new int[side * side];
        for (int i = 0; i < side * side; i++)
        {
            valTest[i] = UnityEngine.Random.Range(1, 257);
        }
        gridCtrl.SetCellValues(valTest);
    }

    string path;
    public void LoadFile()
    {
        path = EditorUtility.OpenFilePanel("Load CSV File", "", "csv");
        if (path.Length != 0)
        {
            var lines = File.ReadLines(path);
            int linesNumber = lines.Count<string>();

            valTest = new int[linesNumber * linesNumber];

            int lineCount = 0;
            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.Peek() >= 0)
                {
                    string[] splitInts = sr.ReadLine().Split(',');
                    for (int i = 0; i < splitInts.Length; i++)
                    {
                        valTest[(lineCount * linesNumber) + i] = Convert.ToInt32(splitInts[i]);
                    }
                    lineCount++;
                }
            }

            gridCtrl.SetCellValues(valTest);
        }
    }

    private IEnumerator SolveSnake7()
    {
        SnakeChallenge asc = new SnakeChallenge(valTest, 7);
        SnakeCouple aCouple = asc.Compute();
        if (aCouple != null)
        {
            Debug.Log(aCouple.ToString());
            gridCtrl.SetSnake(aCouple.SnakeA.cellsIds, Color.red);
            gridCtrl.SetSnake(aCouple.SnakeB.cellsIds, Color.green);
        }
        else
        {
            Debug.Log("Fail");
        }
        
        yield return null;// new WaitForSeconds(waitTime);
    }

    public void SolveSnake7ButtonPressed()
    {
        StartCoroutine(SolveSnake7());
    }
}
