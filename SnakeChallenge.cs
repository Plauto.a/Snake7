﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace snake7
{
    public class SnakeChallenge
    {
        
        public int SnakeSize { get; private set; }
        public int CellsVisited { get; private set; }
        private int[] values;

        public SnakeChallenge(int[] valuesIn, int snkSize)
        {
            values = valuesIn;
            SnakeSize = snkSize;
        }

        private Dictionary<int, List<Snake>> allSnakePits;
        public SnakeCouple Compute()
        {
            allSnakePits = new Dictionary<int, List<Snake>>();
            for (int i = 0; i < values.Length; i++)
            {
                Cell aCell = new Cell(i, values);

                Dictionary<int, List<Snake>> cellSnakePit = aCell.Compute(SnakeSize);

                foreach (KeyValuePair<int, List<Snake>> snakeGroup in cellSnakePit)
                {
                    List<Snake> snakes;
                    if (allSnakePits.TryGetValue(snakeGroup.Key, out snakes))
                    {
                        SnakeCouple aCouple;
                        CheckForCouples(allSnakePits[snakeGroup.Key], cellSnakePit[snakeGroup.Key], out aCouple);
                        if (aCouple != null)
                        {
                            CellsVisited = i;
                            return aCouple;
                        }
                        allSnakePits[snakeGroup.Key].AddRange(snakeGroup.Value);

                    }
                    else
                    {
                        allSnakePits.Add(snakeGroup.Key, snakeGroup.Value);
                    }
                    
                }
            }
            CellsVisited = values.Length;
            return null;
        }

        private bool CheckForCouples(List<Snake> currentSnakes, List<Snake> newSnakes, out SnakeCouple snakeCouple)
        {
            for (int i = 0; i < currentSnakes.Count; i++)
            {
                for (int j = 0; j < newSnakes.Count; j++)
                {
                    if (!Snake.Intersect(currentSnakes[i], newSnakes[j]))
                    {
                        snakeCouple = new SnakeCouple(currentSnakes[i], newSnakes[j]);
                        return true;
                    }
                }
            }
            snakeCouple = null;
            return false;
        }
    }
}
